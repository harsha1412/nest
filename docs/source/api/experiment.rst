Experiment
=======================

Flow
---------------------------------

.. automodule:: nest.experiment.experiment
   :members:
   :undoc-members:
   :show-inheritance:


run_exp
-------------------------------

.. automodule:: nest.experiment.run_exp
   :members:
   :undoc-members:
   :show-inheritance:
